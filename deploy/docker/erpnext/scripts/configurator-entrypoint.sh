echo "set-config start";

ls -1 apps > sites/apps.txt;
bench set-config -g db_host $DB_HOST;
bench set-config -gp db_port $DB_PORT;
bench set-config -g redis_cache "redis://$REDIS_CACHE";
bench set-config -g redis_queue "redis://$REDIS_QUEUE";
bench set-config -g redis_socketio "redis://$REDIS_SOCKETIO"; 
bench set-config -gp socketio_port $SOCKETIO_PORT;

echo "set-config done";

until [[ -n `grep -hs ^ sites/common_site_config.json | jq -r ".db_host // empty"` ]] && \
    [[ -n `grep -hs ^ sites/common_site_config.json | jq -r ".redis_cache // empty"` ]] && \
    [[ -n `grep -hs ^ sites/common_site_config.json | jq -r ".redis_queue // empty"` ]];
do
    echo "Waiting for sites/common_site_config.json to be created";
    sleep 5;
    if (( `date +%s`-start > 120 )); then
    echo "could not find sites/common_site_config.json with required keys";
    exit 1
    fi
done;
echo "sites/common_site_config.json found";

bench new-site --no-mariadb-socket \
    --admin-password=$ERP_ADMIN_PASSWORD \
    --db-root-password=$DATABASE_PASSWORD \
    --install-app erpnext \
    --set-default frontend;

exit 0;