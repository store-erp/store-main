FROM frappe/erpnext:v14.29.2
USER root
RUN apt-get update -y \
    apt-get install gcc python3-dev cups libcups2-dev -y

USER frappe
RUN /home/frappe/frappe-bench/env/bin/pip install pycups python-barcode ppf-datamatrix
RUN mkdir -p /home/frappe/tmp

COPY ./frappe/utils/generate_barcode.py /home/frappe/frappe-bench/apps/frappe/frappe/utils/generate_barcode.py
COPY ./frappe/utils/print_format_raw.py /home/frappe/frappe-bench/apps/frappe/frappe/utils/print_format_raw.py

#hooks
COPY ./frappe/hooks.py /home/frappe/tmp/hooks.py
RUN cat /home/frappe/tmp/hooks.py >> /home/frappe/frappe-bench/apps/erpnext/erpnext/hooks.py

COPY ./scripts/configurator-entrypoint.sh /home/frappe/scripts/configurator-entrypoint.sh;
RUN chmod -x /home/frappe/scripts/configurator-entrypoint.sh;

EXPOSE 631


