import os

from PyPDF2 import PdfWriter

import frappe
from frappe import _
from frappe.core.doctype.access_log.access_log import make_access_log
from frappe.translate import print_language
from frappe.utils.pdf import get_pdf
from frappe.www.printview import get_rendered_raw_commands


@frappe.whitelist()
def print_by_server(
	doctype, name, printer_setting, print_format, doc=None, no_letterhead=0, file_path=None
):
	print_settings = frappe.get_doc("Network Printer Settings", printer_setting)
	try:
		import cups
	except ImportError:
		frappe.throw(_("You need to install pycups to use this feature!"))

	try:
		cups.setServer(print_settings.server_ip)
		cups.setPort(print_settings.port)
		conn = cups.Connection()
		raw_cmd = get_rendered_raw_commands(doctype, name, print_format)
		if not file_path:
			file_path = os.path.join("/", "tmp", f"frappe-pdf-{frappe.generate_hash()}.bin")

		with open(file_path, "w", encoding="utf-8") as file:
			file.write(raw_cmd["raw_commands"])
			file.write('\n')
   
		res_idx = conn.printFile(print_settings.printer_name, file_path, name, {})
		return res_idx
	except OSError as e:
		if (
			"ContentNotFoundError" in e.message
			or "ContentOperationNotPermittedError" in e.message
			or "UnknownContentError" in e.message
			or "RemoteHostClosedError" in e.message
		):
			frappe.throw(_("PDF generation failed"))
	except cups.IPPError:
		frappe.throw(_("Printing failed"))
