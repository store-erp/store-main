import os

import frappe
import base64
import barcode

from frappe import _
from io import BytesIO
from barcode.writer import SVGWriter
from ppf.datamatrix.datamatrix import DataMatrix


no_cache = 1


# dev fdeluca.mail@gmail.com
@frappe.whitelist()
def get_item_datamatrix(code, encode=True):
    svg = ""
    matrix = DataMatrix(code)
    svg = matrix.svg()

    if not encode:
        result = svg
    else:
        result = "data:image/svg+xml;base64," + base64.b64encode(svg.encode('utf-8')).decode('utf-8')
        
    return result


@frappe.whitelist()
def get_item_barcode(code,type, encode=True):
    svg = ""
    if type == 'ean13':
        rv = BytesIO()
        barcode.EAN13(code, writer=SVGWriter(), no_checksum=True).write(rv, {"module_width":0.4, "module_height":5, "font_size": 14, "quiet_zone": 1})
        svg = rv.getvalue().decode("utf-8")

    if not encode:
        result = svg
    else:
        result = "data:image/svg+xml;base64," + base64.b64encode(svg.encode('utf-8')).decode('utf-8')
    return result