#!/bin/bash
MAIN_PATH=/home/services/data
VOLUMES=("erpnext-sites:/www"
        "erpnext-logs:/logs"
        "db-data:/database"
        "redis-queue-data:/redis-queue"
        "redis-socketio-data:/redis-socketio"
        "redis-cache-data:/redis-cache")


if [ "$EUID" -ne 0 ]
  then echo "Please run as root or use sudo"
  exit
fi

echo "========== start =========="
for vol in "${VOLUMES[@]}" ; do
    VLABEL="${vol%%:*}";
    VPATH="${MAIN_PATH}${vol##*:}";
    printf "%s => %s.\n" "$VLABEL" "$VPATH";
    mkdir -p "${VPATH}";
    docker volume create --name $VLABEL --opt type=none --opt device=$VPATH --opt o=bind;
done

echo "========== docker volumes =========="
docker volume ls
echo "========== exit =========="