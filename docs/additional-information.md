# DDNS

> Optional! Only if white ip address not permited

1. install noip client

```bash

mkdir~/Downloads

cd~/Downloads

wgethttps://dmej8g5cpdyqd.cloudfront.net/downloads/noip-duc_3.0.0.tar.gz

tarxfnoip-duc_3.0.0.tar.gz

cd./noip-duc_3.0.0/binaries

sudoaptinstall./noip-duc_3.0.0_amd64.deb

```

2. login into client

```bash

noip-duc-gall.ddnskey.com--username<DDNSKeyUsername>--password<DDNSKeyPassword>

```



# Configuration Wireguard server

1. Install requirements
```bash
sudo apt update
sudo apt install curl bash resolvconf
```

1. Download and install Wireguard script
```bash
curl https://raw.githubusercontent.com/complexorganizations/wireguard-manager/main/wireguard-manager.sh --create-dirs -o /usr/local/bin/wireguard-manager.sh
chmod +x /usr/local/bin/wireguard-manager.sh
wireguard-manager.sh
```

sudo apt-get install wireguard

wget https://git.io/wireguard -O wireguard-install.sh && bash wireguard-install.sh



```
2. Create private key
```bash
wg genkey
```
3. Create public key



# Create remote user
> Only if user didn't create when installing the OS!
 
`remoteuser` - username (more then 10chars)

```shell
adduser remoteuser
usermod -aG sudo remoteuser
```

> Warning!
>
> - На каждом сервере пара логина и пароля должна быть уникальной
> - Обязательно заглавные, строчные и цифры в логине
> - Пароль должен быть более 18 символов
> - Включать заглавные, строчные, цифры и символы
> - Менять пароль не реже 1 раз в 3 месяца

# Docker
1. Remove all volumes
```bash
docker volume rm $(docker volume ls -q)
```


2. Clean data
```bash
cd /home/services/deploy/docker/erpnext
docker compose down
cd /home/services/deploy/docker/databases
docker compose down
rm -R /home/services/data
docker volume rm $(docker volume ls -q)

```