# [[DRAW]]

## Tags
- ERP Next
- Print Server
- Backup
- Restore
- Antivirus

---

# Install OS

- Ubuntu Desktop 23.04
  - minimal setup
  - include proprietary drivers
  - lang: 🇺🇸, 🇺🇦

> Установку рекомендовано проводить на английском языке!
> При выборе украинского языка, нет доступа  переключить язык на английский при вводе данных пользователя

## Upgrade Packages

```sh
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt install curl git
```

Update kernel to last stable build with GUI Focal

### Update snap store

```sh
snap refresh snap-store
# kill 2214
snap refresh snap-store
```

## Install addition software

> This block is optional

- VS Code
- VLC
- Zoom Client
- Brave

### Settings

- Change language with `CapsLock`
- Connect to Google account

# Clone project files
```bash
git clone https://gitlab.com/store-erp/store-main.git /home/services
cd /home/services
```

# Setup remote access
## Open tunnel with ZeroTier
> Optional! Only if white ip address not permited
1. Install zerotier-cli
```bash
mkdir ~/Downloads
cd ~/Downloads
curl -s https://install.zerotier.com | sudo bash
```

2. Join to network
`0a0a0a0a0a0a0a0a0` -network id
```bash                 
sudo zerotier-cli join 0a0a0a0a0a0a0a0a0
```

1. Visit to site and Authorize in Members block
![1710541620863](image/deploy/1710541620863.png)


## Setup SSH server

[manual](https://www.a2hosting.com/kb/getting-started-guide/accessing-your-account/disabling-ssh-logins-for-root/)

1. Install ssh

```sh
sudo apt-get install ssh openssh-client openssh-server
```

2. Write *public ssh key* into authorized keys config
```bash
sudo nano ~/.ssh/authorized_keys
```
Save file `CTRL+S` and close file `CTRL+X`

3. Copy default config file
`USER_NAME` - replace with you remote user login
```bash
cp /home/services/deploy/01-default.conf  /etc/ssh/sshd_config.d/01-default.conf
sed -i 's/REMOTE_USER/USER_NAME/' /etc/ssh/sshd_config.d/01-default.conf

```

4. Configure firewall rules

```bash
ufw allow 2222/tcp # ваш порт 
ufw enable
ufw status
```

5. Restart ssh service

```sh
service ssh restart
service sshd restart
```

6. Reconect with new user
`~/.ssh/private_ssh` - replace with your private key
```sh
ssh -i ~/.ssh/private_ssh -p 22 remoteuser@hostname.local
```

7. Open config file and add uncomment parameter `PasswordAuthentication no`

```bash
nano /etc/ssh/sshd_config.d/01-default.conf
```

## Install endlessh
1. Download and install
```sh
sudo apt install build-essential libc6-dev
cd /tmp
git clone https://github.com/do-community/endlessh
```
2. Check correct work
```sh
sudo ./endlessh -v -p 22
```
3. Open firewall
```sh
ufw allow 22/tcp
```
4. Copy service files and open endlessh.service 
```sh 
mv ./endlessh /usr/local/bin/
setcap 'cap_net_bind_service=+ep' /usr/local/bin/endlessh
cp util/endlessh.service /etc/systemd/system/
nano /etc/systemd/system/endlessh.service
```
Update the file by removing # at the beginning of the line with AmbientCapabilities=CAP_NET_BIND_SERVICE and adding # to the beginning of the line PrivateUsers=true. Like so `/etc/systemd/system/endlessh.service`:
```sh
...
## 2) uncomment following line
AmbientCapabilities=CAP_NET_BIND_SERVICE
## 3) comment following line
#PrivateUsers=true
...
```

5. Init endlessh config
```sh
mkdir /etc/endlessh
echo "Port 22" | tee /etc/endlessh/config
sudo systemctl --now enable endlessh
sudo systemctl status endlessh
``` 

