# Structure
- main path `/home/services/`
- first step `git pull`
```mermaid
graph TD;



DB_STACK[Database Stack]
DB[Maria DB]
ADB[Adminer DB]
DB_STACK-->DB
DB_STACK-->ADB

%% Redis Stack %%
REDIS_STACK[Redis Stack]
REDISC[Redis Cache]
REDISS[Redis SockertIO]
REDISQ[Redis Queue]
REDIS_STACK-->REDISC
REDIS_STACK-->REDISS
REDIS_STACK-->REDISQ

%% Services %%
SERV[Services Stack]
SERV-->Backend
SERV-->Websocket
SERV-->Frontend
SERV-->Queue-long
SERV-->Queue-default
SERV-->Queue-short
SERV-->Queue-scheduler
```

# Docker environment

1. Instal Docker command below or visit to [manual](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)

```bash
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

2. Check Docker Engine work

```bash
sudo docker run hello-world

[Result]
...
Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

3. Deploy portainer agent on server
Portainer agent using `9009` port. Agent need install on worker server.

```sh
docker run -d \
  -p 9009:9009 \
  --name portainer_agent \
  --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /var/lib/docker/volumes:/var/lib/docker/volumes \
  portainer/agent:2.18.2
```



# Create Volumes
Use script for fast create volumes. Script have params `MAIN_PATH` and `VOLUMES`.

```shell
bash /home/services/deploy/docker/create_volumes.sh 
```


# Deploy Database Stack
1. Set params in `.env`
2. Run commands
```bash
cd /home/services/deploy/docker/databases
docker compose up -d
docker compose logs -f -t

```


# Deploy ERPNext Stack
Configurator runs first and update config files. The first launch it creates a new site - `frontend`.  Entrypoint describe in  `deploy/docker/erpnext/scripts/configurator-entrypoint.sh`

1. Set params in `.env`
2. Run commands
```bash
cd /home/services/deploy/docker/erpnext
docker compose up -d
docker compose logs -f -t

```
 Default login name `Administrator`.


